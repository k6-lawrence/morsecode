This is the README file for how to use morsecode...

You will need 2 microbits and morsecodechallenge code.

Setting up the sending microbit:
1. Plug this in and open the finalsendmorse folder 
2. Type "yt target bbc-microbit-classic-gcc"
3. Then type "yt build"
4. Then should type "cp build/bbc-microbit-classic-gcc/source/iot-example-combined.hex /media/student/MICROBIT"
5. Unplug the microbit it's set up for sending

Setting up the recieving microbit:
1. Plug this in and open the finalrecievemorse folder 
2. Type "yt target bbc-microbit-classic-gcc"
3. Then type "yt build"
4. Then should type "cp build/bbc-microbit-classic-gcc/source/iot-example-combined.hex /media/student/MICROBIT"
5. Unplug the microbit it's set up for recieving 

Now they're set up for communication. 

How to connect: 
1. Supply power to both microbits either with USB or with battery packs.
2. Put a crocodile clip on pin 1 of the send and pin 1 of the recieve microbit
3. Put a second crocile clip on the GND pin of the send and the GND pin of the recieve microbit.

They're now connected and ready to communicate.

The Protocol: 
On sending microbit press the A button shortly for a dot, and long press for a dash.
The result will scroll across the LED display.
On the recieving microbit press the A button to add this dot or dash from sending microbit to the list. 

Create a combination of dots or dashes (look up a morsecode table) e.g. lets say we want to create the letter A you use ".-", once your letter is done press B on the receive microbit to display it across the LED display. 

My protocol only allows you to make one character at a time, numbers are also accepted. 
If you mess up your character and want to start again press the reset button on the back of both microbits.



