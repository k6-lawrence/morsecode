#ifndef __YOTTA_BUILD_INFO_H__
#define __YOTTA_BUILD_INFO_H__
// yotta build info, #include YOTTA_BUILD_INFO_HEADER to access
#define YOTTA_BUILD_YEAR 2018 // UTC year
#define YOTTA_BUILD_MONTH 3 // UTC month 1-12
#define YOTTA_BUILD_DAY 6 // UTC day 1-31
#define YOTTA_BUILD_HOUR 11 // UTC hour 0-24
#define YOTTA_BUILD_MINUTE 30 // UTC minute 0-59
#define YOTTA_BUILD_SECOND 10 // UTC second 0-61
#define YOTTA_BUILD_UUID b3e618bf-c594-4abe-b3f9-5ccc97d6ef2b // unique random UUID for each build
#endif // ndef __YOTTA_BUILD_INFO_H__
