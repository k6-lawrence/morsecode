#include "MicroBit.h"
#include "stdlib.h"
#include "vector"
#include "string"
using namespace std;
MicroBit uBit;
MicroBitPin P1 (MICROBIT_ID_IO_P1, MICROBIT_PIN_P1, PIN_CAPABILITY_ALL);
MicroBitButton buttonA(MICROBIT_PIN_BUTTON_A, MICROBIT_ID_BUTTON_A);
//Only button A is utilised on the sending microbit to create ./-
int main()
{
    uBit.init();
    bool pressed = false;
    while(1) {
        // read current number of milliseconds
        uint64_t reading = system_timer_current_time();

        while (buttonA.isPressed()) {
            pressed = true;
        }
        uint64_t delta = system_timer_current_time() - reading;

        //When the user pressed the button use timer to work out if its a . or -
          if (pressed) {
              if (delta > 1000 && delta < 3000) {
                  uBit.display.scroll("-");
                  P1.setDigitalValue(0);
                  uBit.sleep(500);
              }
              else if (delta < 1000) {
                  uBit.display.scroll(".");
                  P1.setDigitalValue(1);
                  uBit.sleep(500);
              }
              pressed = false;
              uBit.display.clear();
              //Clean display for next item
            }
    }

    release_fiber();
}

